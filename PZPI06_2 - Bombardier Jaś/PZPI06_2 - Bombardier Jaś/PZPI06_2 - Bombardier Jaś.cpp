﻿#include <iostream>
#include <map>
#include <vector>
#include <tuple>
#include <algorithm>

const float maxAngle = 360;
const float minAngle = 0;
const float doubleErr = 0.0699999999999;
const float numMaxDig = 1000;

std::map<int, float> getAnglesOfPoints(int n, const std::vector<float> points) {
	std::map<int, float> angles;

	for (auto &i : points) {
		float k = static_cast<float>(i * maxAngle / n);
		if (k > maxAngle)
			break;
		angles[static_cast<int>(i)] = k;
	}

	return angles;
}

std::map<int, float> getAllAngles(int n) {
	std::map<int, float> angles;

	for (int i = 1; ; i++) {
		float k = static_cast<float>(i * maxAngle / n);
		k = (int)(k / 0.001) * 0.001;
		if (k >= maxAngle)
			break;
		angles[static_cast<int>(i)] = k;
	}

	return angles;
}

std::vector<float> getAnglesPointsDiff(const std::map<int, float> &angles) {
	std::vector<float> anglesDiff;
	anglesDiff.push_back(angles.begin()->second + maxAngle - (--angles.end())->second);
	float last = angles.begin()->second;
	for (auto it = std::next(angles.begin()); it != angles.end(); it++) {
		anglesDiff.push_back(it->second - last);
		last = it->second;
	}

	return anglesDiff;
}

//checking equal of float untill their second digit
bool isEqual(float x, float y) {
	x *= numMaxDig; y *= numMaxDig;
	if (static_cast<int>(x) == static_cast<int>(y))
		return true;
	return false;
}

//first float is sum of diff, second is max diff
std::tuple<float, float> getSmallestOfDiv(const std::vector<float> &anglesDiff, int startKey) {
	float sum = 0.0;
	float max = minAngle;
	int blocker = startKey - 1 >= 0 ? startKey - 1 : anglesDiff.size() - 1;

	for (int i = 0; i < anglesDiff.size(); i++) {
		if (i != blocker) {
			sum += anglesDiff[i];
			if (anglesDiff[i] > max)
				max = anglesDiff[i];
		}
	}

	return std::tuple<float, float>(sum, max);
}

int getIdOfSumMax(const std::vector<float>& anglesDiff, std::map<int, float> angles, int startPoint) {
	auto  sum_max = getSmallestOfDiv(anglesDiff, startPoint);

	int sumId, maxId;
	sumId = maxId = (--angles.end())->first + 1; //n
	float sum = std::get<0>(sum_max);
	float max = std::get<1>(sum_max);

	for (auto& x : angles) {
		float t = sum / (x.second * static_cast<int>(sum / x.second));
		if (t - 1 < doubleErr && sum - doubleErr >= x.second && max - doubleErr <= x.second)
			sumId = x.first;

		if(isEqual(max, x.second) && x.first < maxId)
			maxId = x.first;
	}

	return sumId < maxId ? sumId : maxId;
}

int getCoreTime(int n, std::vector<float> points) {
	auto angles = getAnglesOfPoints(n, points);
	auto allAngles = getAllAngles(n);
	auto anglesDiff = getAnglesPointsDiff(angles);
	int smallestOne = (--allAngles.end())->first;

	for (int i = 0; i < anglesDiff.size(); i++) {
		int x = getIdOfSumMax(anglesDiff, allAngles, i);
		if (x < smallestOne)
			smallestOne = x;
	}

	return smallestOne;
}

int main()
{
	int t, n, m, k;
	std::vector<float> points;
	std::cin >> t;
	for (int i = 0; i < t; i++) { 
		std::cin >> m >> n;
		for (int j = 0; j < m; j++) {
			std::cin >> k;
			points.push_back(k);
		}
		std::cout << getCoreTime(n, points) << std::endl;
		points.clear();
	}
}