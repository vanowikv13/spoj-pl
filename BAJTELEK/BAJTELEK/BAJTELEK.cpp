#include "stdafx.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <string>

using namespace std;

typedef vector<pair<long long, long long>> pointsArr;
typedef pair<long long, long long> ppair;

void insertPoints(pointsArr &points, string &pointsLine) {
	string number[2]; short numberStatus = 0;
	for (int i = 0; i < pointsLine.size(); i++) {
		if (pointsLine[i] == ' ' && numberStatus == 1 && number[0] != "") {
			points.push_back(ppair(stoi(number[0]), stoi(number[1])));
			number[0] = number[1] = "";
			numberStatus = 0;
		}
		else if (numberStatus == 0 && pointsLine[i] == ' ')
			numberStatus = 1;
		else
			number[numberStatus] += pointsLine[i];
	}
	//because at the end there is no space so we have to do it manualy
	if(number[0] != "" && number[1] != "")
		points.push_back(ppair(stoi(number[0]), stoi(number[1])));
	//change value of first element
	auto it = points.end(); it -= 2;
	points[0].first = it->first;
	points[0].second = it->second;

}

void writePoints(pointsArr points) {
	for (auto &x : points)
		cout << x.first << " " << x.second << endl;
}

long double countArea(pointsArr points) {
	long double sum = 0;
	for (int i = 1; i < points.size() - 1; i++)
		sum += points[i].second * (points[i - 1].first - points[i + 1].first);
	sum = abs(sum);
	sum = sum / 2;
	return sum;
}

int main()
{
	//structure for our data with first initialization
	long long x;
	string line[2];
	long double count[2];
	cin >> x;
	cin.ignore();
	for (int i = 0; i < x; i++) {

		getline(cin, line[0]);
		pointsArr points1 = { ppair(0,0) };
		if (line[0].size() > 3) {
			insertPoints(points1, line[0]);
			count[0] = countArea(points1);
			points1.clear();
		}

		getline(cin, line[1]);
		pointsArr points2 = { ppair(0,0) };
		if (line[1].size() > 3) {
			insertPoints(points2, line[1]);
			count[1] = countArea(points2);
			points2.clear();
		}

		//empty line between programs
		cout << static_cast<long long>((count[1] - count[0]) * 6) + static_cast<long long>(count[0] * 10) << endl;
		getline(cin, line[0]);
	}

	system("pause");
    return 0;
}

